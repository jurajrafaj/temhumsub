#include "MyParser.h"

MyParser::MyParser(byte* payload, int length) {
  char buffer[20];
  for (int i = 0; i < length; i++) {
    buffer[i] = (char)payload[i];
  }
  
  temperature = atoi(buffer);
  
  char* comma = strchr(buffer, ',');
  humidity = atoi(buffer);
}

int MyParser::getTemperature() {
  return temperature;
}

int MyParser::getHumidity() {
  return humidity;
}