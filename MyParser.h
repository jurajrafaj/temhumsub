#ifndef __MYPARSER_H__
#define __MYPARSER_H__

#include <Arduino.h>

class MyParser {
private:
  int temperature;
  int humidity;

public:
  MyParser(byte* payload, int length);
  int getTemperature();
  int getHumidity();
};

#endif