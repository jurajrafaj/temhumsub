#include <MyParser.h>
#include <NTPClient.h>
#include <PubSubClient.h>
#include <WiFi.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <WiFiUdp.h>
#include <WROVER_KIT_LCD.h>


const char* ssid = "stage3";
const char* pass = "stage123!";
const char* mqtt_server = "192.168.223.115";
const int   mqtt_port = 1883;

WiFiClient client;
PubSubClient mqttClient(client);

void setup() {
  Serial.begin(9600);
  connect_to_wifi();
  mqttClient.setServer(mqtt_server, mqtt_port);
  mqttClient.setCallback(callback);
}

void loop() {
  if (!mqttClient.connected()) {
    if (mqttClient.connect("esp32client")) {
      mqttClient.subscribe("/mydevice/telemetry");
    } 
    else {
      delay(5000);
    }
  }
  mqttClient.loop();
}
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.println("message arrived");
  Serial.print("  -> topic: ");
  Serial.println(topic);
  
  MyParser parser(payload, length);
  Serial.print("  -> temperature: ");
  Serial.println(parser.getTemperature());
  Serial.print("  -> humidity: ");
  Serial.println(parser.getTemperature());
}

void connect_to_wifi(){
  Serial.begin(115200);
  Serial.println("Connecting, please wait...");
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.print(".");
    }
  Serial.println("");
  Serial.println("Connecting successful!");
  Serial.println("Received IP adress: ");
  Serial.println(WiFi.localIP());
}